import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtMultimedia 5.6

import "../models"
import "../elements"
import "../utils.js" as Utils
/**
  Video Player Page

  this page MUST be loaded with video object in globals.currentVideo
*/
Page {
    id: page

    title: globals.currentVideo.meta.name

    palette{window:  "black"}


    Component.onCompleted: {
        if (settings.autoPlay) videoPlayer.playPause();
    }

    Video {
        property bool isPlaying: false
        property variant currentFile: undefined

        id: videoPlayer
        anchors.fill: parent;

        onPlaying: isPlaying = true
        onPaused: isPlaying = false

        source: ""

        onSourceChanged: console.log("video source:", source)

        onCurrentFileChanged: currentFile ? source = currentFile.fileUrl : ""

        onStatusChanged: {
            console.log("onStatusChanged", status);
        }

        onErrorChanged: {
            console.log("onErrorChanged", error, errorString);
        }

        function playPause() {
            if ( isPlaying ) {
                pause();
            } else {
                if (currentFile === undefined) currentFile = globals.currentVideo.getFile(settings.preferredQuality);
                if (currentFile === undefined)  {
                    console.log("No playable source!")
                    return
                }

                play();
            }
        }
    }

    Image {
        id: poster
        anchors.fill: parent
        source: globals.instance.host + globals.currentVideo.meta.previewPath
        fillMode: Image.PreserveAspectFit
        visible: videoPlayer.status <  MediaPlayer.Loaded
    }

    Item {
        id: errorMessage
        anchors.centerIn: parent
        width: parent.width
        height: errorLabel.height + (theme.paddingMedium * 2)

        visible: videoPlayer.error !== MediaPlayer.NoError

        Rectangle {
            color: "#000000"
            anchors.fill: parent;
        }
        Label {
            id: errorLabel
            anchors.centerIn: parent
            width: parent.width
            wrapMode: Text.WordWrap
            text: qsTr("Error:") + videoPlayer.errorString
            color: "#FFFFFF"
            horizontalAlignment: Text.AlignHCenter
        }

    }

    header: ToolBar {
        palette {
            window: window.palette.window
        }

        RowLayout {
            anchors.fill: parent
            ToolButton {
                text: qsTr("Stop")
                icon {
                    name: "media-playback-stop"
                    source: "/icons/svgicons.stop.png"
                }
                onClicked: stackView.pop()
            }
            ToolButton {
                text: videoPlayer.isPlaying ? qsTr("Pause") : qsTr("Play")
                icon {
                    source: videoPlayer.isPlaying ? "/icons/svgicons.pause.png" : "/icons/svgicons.play2.png"
                    name: videoPlayer.isPlaying ? "media-playback-pause" : "media-playback-start"
                }
                onClicked: videoPlayer.playPause()
            }
            Slider {
                Layout.fillWidth: true
                from: 0
                to: globals.currentVideo.meta.duration * 1000
                value: videoPlayer.position
                enabled: false
            }
            Label {
                text: Utils.s2str(videoPlayer.position/1000)
            }
            ComboBox {
                property var files: globals.currentVideo.meta.files
                onFilesChanged: {
                    var newmodel = [];
                    var currentid = 0;
                    if (!files) return;
                    for (var key in files) {
                        var f = files[key];
                        if (videoPlayer.currentFile && f.resolution.id === videoPlayer.currentFile.resolution.id) currentid = key;
                        if (!videoPlayer.currentFile && f.resolution.id === settings.preferredQuality) currentid = key;
                        newmodel.push(f.resolution.label)
                    }
                    model = newmodel;
                    currentIndex = currentid

                }
                model: []
                onModelChanged: console.log(JSON.stringify(model))
                onActivated: {
                    videoPlayer.pause()
                    videoPlayer.currentFile = globals.currentVideo.meta.files[currentIndex]
                }
            }
        }
    }

    footer: ProgressBar {
        indeterminate: true
        visible: videoPlayer.status <  MediaPlayer.Loaded
    }


}
