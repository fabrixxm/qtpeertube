import QtQuick 2.4
import QtQuick.Controls 2.3

import "../elements"

Page {
    id: page

    title: "Instances"

    header: ToolBar {

    }

    ScrollView {
        width: 400
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            top: parent.top
            topMargin: theme.paddingLarge
        }
        ListView {
            id: listView
            model: settings.bookmarks
            delegate: InstanceItem{
                onClicked: {
                    globals.instance = modelData;
                    stackView.push("Videos.qml")
                }
            }
        }
    }
}
