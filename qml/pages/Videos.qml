import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import "../models"
import "../elements"

Page {
    id: page

    property bool inSearch: false

    title: globals.instance.name + (inSearch ? qsTr(" (search results)") : "")

    VideoData {
        id: video
        onLoaded: {
            globals.currentVideo = video;
            videoDetails.open();
        }
    }

    SearchModel {
        id: searchModel
    }

    VideoListModel{
      id: videoListModel
    }

    header: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                icon {
                    name: "go-previous"
                    source: "/icons/svgicons.back.png"
                }
                text: qsTr("Back")
                onClicked: {
                    globals.currentVideo = null;
                    globals.instance = {"name":"", "host":""};
                    stackView.pop();
                }
            }
            TextField {
                Layout.fillWidth: page.inSearch
                placeholderText: qsTr("Search")
                onEditingFinished: {
                    if (text==="") {
                        listView.model = videoListModel.model
                        page.inSearch = false
                    } else {
                        searchModel.search = text
                        listView.model = searchModel.model
                        page.inSearch = true
                    }
                }
            }

            Rectangle {
                Layout.fillWidth: true
                visible: !page.inSearch
            }

            Switch {
                text: qsTr("Only local")
                onToggled:
                    if (checked) {
                        videoListModel.filterLocal()
                    } else {
                        videoListModel.filterAll()
                    }
                visible: !page.inSearch
            }

            ComboBox {
                model: ["Trending", "Recent"]
                onActivated: {
                    if (currentIndex === 0) videoListModel.orderTrending()
                    if (currentIndex === 1) videoListModel.orderRecent()
                }
            }
        }

    }

    footer: ProgressBar {
        indeterminate: true
        visible: videoListModel.status === 0 || video.status === 0
    }


    ScrollView {
        anchors.fill: parent
        anchors {
            topMargin: theme.paddingMedium
            leftMargin: theme.paddingMedium
        }

        GridView {
            id: listView
            boundsBehavior: Flickable.DragOverBounds
            model: videoListModel.model
            cellWidth: 100 + theme.paddingMedium
            cellHeight: 100 + theme.paddingMedium
            delegate: VideoGridItem{
                width: listView.cellWidth
                height: listView.cellHeight
                onClicked: {
                    if (video.uuid === model.uuid) videoDetails.open();
                    video.uuid = model.uuid
                    //videoDetails.open();
                }
                /*onVideoplay: {
                    video.uuid = model.uuid
                    //page.videoPlay()
                }*/
            }
        }
    }

    VideoDetails {
        id: videoDetails
        y: header.height
        height: window.height - header.height
        onVideoplay: {
            videoDetails.close()
            page.videoPlay()
        }
    }

    function videoPlay() {
        stackView.push("VideoPlayer.qml")
    }
}
