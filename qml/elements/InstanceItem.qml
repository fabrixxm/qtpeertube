import QtQuick 2.0
import QtQuick.Controls 2.3

ItemDelegate {
    anchors {
        left: parent.left
        right: parent.right
    }
    height: 48 + (theme.paddingSmall * 2)


    icon {
        name: "video-display"
        source: "/icons/svgicons.computer.png"
    }

    Column {
        id: column

        anchors {
            left:parent.left
            leftMargin: 48
            verticalCenter: parent.verticalCenter
        }
        Label {
            text: modelData.name
            font.bold: true
        }
        Label {
            text: modelData.host
        }
    }
}
