import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import "../utils.js" as Utils
import "../timeago.js" as TimeAgo

ItemDelegate {
    id: control
    /*width: 100
    height: 80*/


    signal videoplay()

    clip: true
    contentItem:  Column {
        id: column

        VideoThumb {
            width: parent.width
            url:  model.thumbnailPath
            duration: model.duration
            isNSFW: model.nsfw

            onVideoplay: control.videoplay()
        }

        Label {
            id: name
            text: model.name
            font.bold: true
            width: control.availableWidth
            wrapMode: Text.WordWrap
        }

        Label {
            id: publish
            text: TimeAgo.format(Utils.json2date(model.publishedAt))
        }

        /*
        Label {
            id: description
            text: model.description ? model.description : ""
            width: control.availableWidth
            wrapMode: Text.WordWrap
        }
        */
    }


    background: Rectangle {
        anchors.fill: parent
        color: control.down ?  window.palette.highlight : window.palette.window
    }

}

