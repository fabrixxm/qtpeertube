import QtQuick 2.0
import QtGraphicalEffects 1.0

import "../utils.js" as Utils

Item {
    id: item
    property string url
    property int duration
    property bool isNSFW: false
    property bool playable: false

    signal videoplay()

    height: width * 9/16

    Image {
        id: img
        source: !url ? "" : (globals.instance.host + url)
        anchors.fill: parent
    }

    FastBlur {
        visible: isNSFW
        anchors.fill: img
        source: img
        radius: 50
    }

    Rectangle {
        color: "#000000"
        width: txt.width + theme.paddingSmall*2
        height: txt.height + theme.paddingSmall*2
        anchors {
            right: parent.right
            bottom: parent.bottom
            rightMargin: theme.paddingSmall
            bottomMargin: theme.paddingSmall
        }
    }

    Text {
        id: txt
        color: "white"
        font.pixelSize: item.width * 0.06
        text: duration ? Utils.s2str(duration) : ""
        anchors {
            right: parent.right
            bottom: parent.bottom
            rightMargin: theme.paddingSmall * 2
            bottomMargin: theme.paddingSmall * 2
        }
    }

    Image {
        id: playimg
        source: "/icons/svgicons.play.png"
        anchors.centerIn: parent
        visible: playable
    }

    MouseArea {
        enabled: playable
        hoverEnabled: true
        anchors.fill: parent;
        onClicked: item.videoplay()
        onContainsMouseChanged: {
            parent.state = containsMouse ? "on" : "off"
        }
    }

    state: "off"
    states: [
        State {
            name: "off"
            PropertyChanges { target: playimg; opacity: 0.6; }
        },

        State {
            name: "on"
            PropertyChanges { target: playimg; opacity: 1.0; }
        }
    ]
    transitions: [
        Transition {
            SequentialAnimation{
                NumberAnimation { properties: "opacity"; duration: kStandardAnimationDuration }
            }
        }
    ]

}

