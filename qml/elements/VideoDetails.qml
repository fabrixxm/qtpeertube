import QtQuick 2.0
import QtQuick.Controls 2.3

import "../utils.js" as Utils

Drawer {
    id: control
    edge: Qt.RightEdge
    dragMargin: 0
    width: 300;
    height: window.height

    signal videoplay()

    contentItem:
        ScrollView {
            width: control.width
            height: control.height
            clip: true

            Flickable {
                boundsBehavior: Flickable.DragOverBounds
                flickableDirection: Flickable.VerticalFlick

                Column {
                    id: column
                    spacing: theme.paddingMedium
                    width: control.width-1

                    VideoThumb {
                        width: column.width
                        url:  !globals.currentVideo ? "" : globals.currentVideo.meta.previewPath
                        duration: !globals.currentVideo ? 0 : globals.currentVideo.meta.duration

                        playable: true
                        onVideoplay: control.videoplay()
                    }

                    Label {
                        id: name
                        text: !globals.currentVideo ? "" : globals.currentVideo.meta.name
                        font.bold: true
                        anchors {
                            left: parent.left
                            right: parent.right
                            leftMargin: theme.paddingMedium
                            rightMargin: theme.paddingMedium
                        }
                        wrapMode: Text.WordWrap
                    }

                    Separator {}

                    Row {
                        anchors {
                            left: parent.left
                            right: parent.right
                            leftMargin: theme.paddingMedium
                            rightMargin: theme.paddingMedium
                        }
                        spacing: theme.paddingMedium

                        Row {
                            Image { source : "/icons/svgicons.eye.png"; width: 16; height: 16; anchors.verticalCenter: parent.verticalCenter}
                            Label { text: !globals.currentVideo ? "" : globals.currentVideo.meta.views; anchors.verticalCenter: parent.verticalCenter }
                        }

                        Row {
                            Image { source : "/icons/svgicons.heart.png"; width: 16; height: 16; anchors.verticalCenter: parent.verticalCenter }
                            Label { text: !globals.currentVideo ? "" : globals.currentVideo.meta.likes; anchors.verticalCenter: parent.verticalCenter }
                        }
                        Row {
                            Image { source : "/icons/svgicons.calendar.png"; width: 16; height: 16; anchors.verticalCenter: parent.verticalCenter }
                            Label { text: !globals.currentVideo ? "" : Utils.json2date(globals.currentVideo.meta.publishedAt).toLocaleString(Qt.locale()); anchors.verticalCenter: parent.verticalCenter }
                        }
                    }


                    Separator {}

                    Label {
                        id: description
                        text: !globals.currentVideo ? "" : globals.currentVideo.meta.description
                        anchors {
                            left: parent.left
                            right: parent.right
                            leftMargin: theme.paddingMedium
                            rightMargin: theme.paddingMedium
                        }
                        wrapMode: Text.WordWrap
                    }
                }
            }
        }

}
