import QtQuick 2.4


Rectangle {
    anchors {
        left: parent.left
        right: parent.right
        leftMargin: theme.paddingMedium
        rightMargin: theme.paddingMedium
    }
    height: 1
    color: window.palette.mid
}

