import QtQuick 2.10
import QtQuick.Controls 2.3
import Qt.labs.settings 1.0

import "./elements"

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: "PeerTube - " + stackView.currentItem.title

    property int kStandardAnimationDuration: 300

    Item {
        id: theme
        property int paddingLarge: 50
        property int paddingMedium: 10
        property int paddingSmall: 2
    }

    Item {
        id: globals;
        property var instance : {"name":"", "host":""}
        property string apiurl: instance.host+"/api/v1"
        property variant currentVideo: null

        property int paginationCount: 20
    }

    Settings {
        id: settings
        property alias width: window.width
        property alias height: window.height

        property int preferredQuality: 480
        property bool autoPlay: true

        property var bookmarks: [{"name":"PeerTube.video","host":"https://peertube.video"},{"name":"peertube.social","host":"https://peertube.social"},{"name":"Blender","host":"https://video.blender.org"}]

        function resetBookmarks() {
            bookmarks = [{"name":"PeerTube.video","host":"https://peertube.video"},{"name":"peertube.social","host":"https://peertube.social"},{"name":"Blender","host":"https://video.blender.org"}];
        }

        function addBookmark(obj) {
            bookmarks.push(obj)
        }

        function removeBookmark(idx) {
            bookmarks.splice(idx, 1);
        }

        Component.onCompleted: {
            resetBookmarks();
        }

    }


    StackView {
        id: stackView
        initialItem: "pages/Instances.qml"
        anchors.fill: parent
    }



}
