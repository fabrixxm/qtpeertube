import QtQuick 2.0

JSONListModel {
    property string sort: "-trending"
    property string filter: ""

    source: globals.apiurl + "/videos/?sort=" + sort + (filter === "" ? "" : "&filter=" + filter)
    query: "$.data[*]"


    function orderTrending() { start = 0; sort = "-trending";  }
    function orderRecent() { start = 0; sort = "-publishedAt"; }

    function filterAll() { start = 0; filter = ""; }
    function filterLocal() { start = 0; filter = "local"; }

}

/*  overview @ /overviews/videos */
