import QtQuick 2.0
import "jsonpath.js" as JSONPath

Item {
    property string source: ""
    property string json: ""
    property string query: ""

    property variant meta

    property int status: -1

    signal loaded()

    onSourceChanged: {
        console.log("onSourceChanged", source);
        status = 0;
        var xhr = new XMLHttpRequest;
        xhr.open("GET", source);
        xhr.onreadystatechange = function() {
            status = xhr.status;
            if (xhr.readyState === XMLHttpRequest.DONE)
                json = xhr.responseText;
        }
        xhr.send();
    }

    onJsonChanged: updateJSONModel()
    onQueryChanged: updateJSONModel()

    function updateJSONModel() {
        if ( json === "" ) {
            thedata = {};
            return;
        }
        meta = parseJSONString(json, query);

        loaded();
    }

    function parseJSONString(jsonString, jsonPathQuery) {
        var obj = JSON.parse(jsonString);
        if ( jsonPathQuery !== "" )
            obj = JSONPath.jsonPath(obj, jsonPathQuery);

        return obj;
    }

}
