import QtQuick 2.0

JSONListModel {
    property string search: ""
    source: search !== "" ? globals.apiurl + "/search/videos?search="+encodeURI(search) : ""
    query: "$.data[*]"

    onSearchChanged: start = 0;
}
