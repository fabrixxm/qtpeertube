import QtQuick 2.0
import "jsonpath.js" as JSONPath

JSONObjectData {
    property string uuid: ""
    source: uuid==="" ? "" : globals.apiurl+"/videos/"+uuid


    /**
     * return the file with a resolution equal or lower to the wanted
     *
     * if no resolution match, get the lowest possible.
     */
    function getFile(max_resolution) {

        if (meta.files === undefined) return undefined;

        console.log("getFile", max_resolution);
        var r = JSONPath.jsonPath(meta, "$.files[?(@.resolution.id <= "+max_resolution+")]");
        // console.log("->", JSON.stringify(r));
        if (r===false) {
            console.log("no resolution found. get last");
            r = JSONPath.jsonPath(meta, "$.files[-1:]");
            console.log("->", JSON.stringify(r));
        }

        return r[0];
    }
}
