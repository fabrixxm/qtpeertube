# QtPeerTube

A QML-based desktop player for PeerTube

## NOTE:

Only play, no interaction, no webtorrent.


## Requirements

    qt5 - a least 5.10
    qt5-multimedia
    qt5-graphicaleffects
