#!/bin/bash
for f in *.svg; do inkscape --export-png="${f/.svg/.png}" -w48 -h48 --export-area-page "$f"; done
